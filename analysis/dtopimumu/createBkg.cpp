void createBkg(){

  TChain in_chain1("h1");
  TChain in_chain2("h2");
  TChain in_chain3("h3");
  in_chain1.Add("dtopimumu_5.root/h1");
  in_chain2.Add("dtopimumu_5.root/h2");
  in_chain3.Add("dtopimumu_5.root/h3");

  TFile* ofile = new TFile("bkg.root","RECREATE");
  TTree* Mu = new TTree("Mu", "Mu Data");
  TTree* E = new TTree("E", "E Data");
  TTree* Pi = new TTree("Pi", "Pi Data");

  float mu_tag = 0, e_tag = 1, k_tag = 2, pi_tag = 3;

  float Deltam,
    Dpmass,
    Pipmass,
    Dmass,
    Pimass,
    Lpmass,
    Lmmass,
    Dpmom,
    Pipmom,
    Dmom,
    Pimom,
    Lpmom,
    Lmmom,
    Dpidhep,
    Pipidhep,
    Didhep,
    Piidhep,
    Lpidhep,
    Lmidhep,
    Lpmuid,
    Lmmuid,
    Lpeid,
    Lmeid;

  in_chain1.SetBranchAddress("Deltam",&Deltam);
  in_chain1.SetBranchAddress("Dpmass",&Dpmass);
  in_chain1.SetBranchAddress("Pipmass",&Pipmass);
  in_chain1.SetBranchAddress("Dmass",&Dmass);
  in_chain1.SetBranchAddress("Pimass",&Pimass);
  in_chain1.SetBranchAddress("Lpmass",&Lpmass);
  in_chain1.SetBranchAddress("Lmmass",&Lmmass);
  in_chain1.SetBranchAddress("Dpmom",&Dpmom);
  in_chain1.SetBranchAddress("Pipmom",&Pipmom);
  in_chain1.SetBranchAddress("Dmom",&Dmom);
  in_chain1.SetBranchAddress("Pimom",&Pimom);
  in_chain1.SetBranchAddress("Lpmom",&Lpmom);
  in_chain1.SetBranchAddress("Lmmom",&Lmmom);
  in_chain1.SetBranchAddress("Dpidhep",&Dpidhep);
  in_chain1.SetBranchAddress("Pipidhep",&Pipidhep);
  in_chain1.SetBranchAddress("Didhep",&Didhep);
  in_chain1.SetBranchAddress("Piidhep",&Piidhep);
  in_chain1.SetBranchAddress("Lpidhep",&Lpidhep);
  in_chain1.SetBranchAddress("Lmidhep",&Lmidhep);
  in_chain1.SetBranchAddress("Lpmuid",&Lpmuid);
  in_chain1.SetBranchAddress("Lmmuid",&Lmmuid);
  in_chain1.SetBranchAddress("Lpeid",&Lpeid);
  in_chain1.SetBranchAddress("Lmeid",&Lmeid);

  in_chain2.SetBranchAddress("Deltam",&Deltam);
  in_chain2.SetBranchAddress("Dpmass",&Dpmass);
  in_chain2.SetBranchAddress("Pipmass",&Pipmass);
  in_chain2.SetBranchAddress("Dmass",&Dmass);
  in_chain2.SetBranchAddress("Pimass",&Pimass);
  in_chain2.SetBranchAddress("Lpmass",&Lpmass);
  in_chain2.SetBranchAddress("Lmmass",&Lmmass);
  in_chain2.SetBranchAddress("Dpmom",&Dpmom);
  in_chain2.SetBranchAddress("Pipmom",&Pipmom);
  in_chain2.SetBranchAddress("Dmom",&Dmom);
  in_chain2.SetBranchAddress("Pimom",&Pimom);
  in_chain2.SetBranchAddress("Lpmom",&Lpmom);
  in_chain2.SetBranchAddress("Lmmom",&Lmmom);
  in_chain2.SetBranchAddress("Dpidhep",&Dpidhep);
  in_chain2.SetBranchAddress("Pipidhep",&Pipidhep);
  in_chain2.SetBranchAddress("Didhep",&Didhep);
  in_chain2.SetBranchAddress("Piidhep",&Piidhep);
  in_chain2.SetBranchAddress("Lpidhep",&Lpidhep);
  in_chain2.SetBranchAddress("Lmidhep",&Lmidhep);
  in_chain2.SetBranchAddress("Lpmuid",&Lpmuid);
  in_chain2.SetBranchAddress("Lmmuid",&Lmmuid);
  in_chain2.SetBranchAddress("Lpeid",&Lpeid);
  in_chain2.SetBranchAddress("Lmeid",&Lmeid);

  in_chain3.SetBranchAddress("Deltam",&Deltam);
  in_chain3.SetBranchAddress("Dpmass",&Dpmass);
  in_chain3.SetBranchAddress("Pipmass",&Pipmass);
  in_chain3.SetBranchAddress("Dmass",&Dmass);
  in_chain3.SetBranchAddress("Pimass",&Pimass);
  in_chain3.SetBranchAddress("Lpmass",&Lpmass);
  in_chain3.SetBranchAddress("Lmmass",&Lmmass);
  in_chain3.SetBranchAddress("Dpmom",&Dpmom);
  in_chain3.SetBranchAddress("Pipmom",&Pipmom);
  in_chain3.SetBranchAddress("Dmom",&Dmom);
  in_chain3.SetBranchAddress("Pimom",&Pimom);
  in_chain3.SetBranchAddress("Lpmom",&Lpmom);
  in_chain3.SetBranchAddress("Lmmom",&Lmmom);
  in_chain3.SetBranchAddress("Dpidhep",&Dpidhep);
  in_chain3.SetBranchAddress("Pipidhep",&Pipidhep);
  in_chain3.SetBranchAddress("Didhep",&Didhep);
  in_chain3.SetBranchAddress("Piidhep",&Piidhep);
  in_chain3.SetBranchAddress("Lpidhep",&Lpidhep);
  in_chain3.SetBranchAddress("Lmidhep",&Lmidhep);
  in_chain3.SetBranchAddress("Lpmuid",&Lpmuid);
  in_chain3.SetBranchAddress("Lmmuid",&Lmmuid);
  in_chain3.SetBranchAddress("Lpeid",&Lpeid);
  in_chain3.SetBranchAddress("Lmeid",&Lmeid);

  Mu -> Branch("Deltam", &Deltam, "Deltam/F");
  Mu -> Branch("Dpmass", &Dpmass, "Dpmass/F");
  Mu -> Branch("Pipmass", &Pipmass, "Pipmass/F");
  Mu -> Branch("Dmass", &Dmass, "Dmass/F");
  Mu -> Branch("Pimass", &Pimass, "Pimass/F");
  Mu -> Branch("Lpmass", &Lpmass, "Lpmass/F");
  Mu -> Branch("Lmmass", &Lmmass, "Lmmass/F");
  Mu -> Branch("Dpmom", &Dpmom, "Dpmom/F");
  Mu -> Branch("Pipmom", &Pipmom, "Pipmom/F");
  Mu -> Branch("Dmom", &Dmom, "Dmom/F");
  Mu -> Branch("Pimom", &Pimom, "Pimom/F");
  Mu -> Branch("Lpmom", &Lpmom, "Lpmom/F");
  Mu -> Branch("Lmmom", &Lmmom, "Lmmom/F");
  Mu -> Branch("Dpidhep", &Dpidhep, "Dpidhep/F");
  Mu -> Branch("Pipidhep", &Pipidhep, "Pipidhep/F");
  Mu -> Branch("Didhep", &Didhep, "Didhep/F");
  Mu -> Branch("Piidhep", &Piidhep, "Piidhep/F");
  Mu -> Branch("Lpidhep", &Lpidhep, "Lpidhep/F");
  Mu -> Branch("Lmidhep", &Lmidhep, "Lmidhep/F");
  Mu -> Branch("Lpmuid", &Lpmuid, "Lpmuid/F");
  Mu -> Branch("Lmmuid", &Lmmuid, "Lmmuid/F");
  Mu -> Branch("Lpeid", &Lpeid, "Lpeid/F");
  Mu -> Branch("Lmeid", &Lmeid, "Lmeid/F");

  E -> Branch("Deltam", &Deltam, "Deltam/F");
  E -> Branch("Dpmass", &Dpmass, "Dpmass/F");
  E -> Branch("Pipmass", &Pipmass, "Pipmass/F");
  E -> Branch("Dmass", &Dmass, "Dmass/F");
  E -> Branch("Pimass", &Pimass, "Pimass/F");
  E -> Branch("Lpmass", &Lpmass, "Lpmass/F");
  E -> Branch("Lmmass", &Lmmass, "Lmmass/F");
  E -> Branch("Dpmom", &Dpmom, "Dpmom/F");
  E -> Branch("Pipmom", &Pipmom, "Pipmom/F");
  E -> Branch("Dmom", &Dmom, "Dmom/F");
  E -> Branch("Pimom", &Pimom, "Pimom/F");
  E -> Branch("Lpmom", &Lpmom, "Lpmom/F");
  E -> Branch("Lmmom", &Lmmom, "Lmmom/F");
  E -> Branch("Dpidhep", &Dpidhep, "Dpidhep/F");
  E -> Branch("Pipidhep", &Pipidhep, "Pipidhep/F");
  E -> Branch("Didhep", &Didhep, "Didhep/F");
  E -> Branch("Piidhep", &Piidhep, "Piidhep/F");
  E -> Branch("Lpidhep", &Lpidhep, "Lpidhep/F");
  E -> Branch("Lmidhep", &Lmidhep, "Lmidhep/F");
  E -> Branch("Lpmuid", &Lpmuid, "Lpmuid/F");
  E -> Branch("Lmmuid", &Lmmuid, "Lmmuid/F");
  E -> Branch("Lpeid", &Lpeid, "Lpeid/F");
  E -> Branch("Lmeid", &Lmeid, "Lmeid/F");

  Pi -> Branch("Deltam", &Deltam, "Deltam/F");
  Pi -> Branch("Dpmass", &Dpmass, "Dpmass/F");
  Pi -> Branch("Pipmass", &Pipmass, "Pipmass/F");
  Pi -> Branch("Dmass", &Dmass, "Dmass/F");
  Pi -> Branch("Pimass", &Pimass, "Pimass/F");
  Pi -> Branch("Lpmass", &Lpmass, "Lpmass/F");
  Pi -> Branch("Lmmass", &Lmmass, "Lmmass/F");
  Pi -> Branch("Dpmom", &Dpmom, "Dpmom/F");
  Pi -> Branch("Pipmom", &Pipmom, "Pipmom/F");
  Pi -> Branch("Dmom", &Dmom, "Dmom/F");
  Pi -> Branch("Pimom", &Pimom, "Pimom/F");
  Pi -> Branch("Lpmom", &Lpmom, "Lpmom/F");
  Pi -> Branch("Lmmom", &Lmmom, "Lmmom/F");
  Pi -> Branch("Dpidhep", &Dpidhep, "Dpidhep/F");
  Pi -> Branch("Pipidhep", &Pipidhep, "Pipidhep/F");
  Pi -> Branch("Didhep", &Didhep, "Didhep/F");
  Pi -> Branch("Piidhep", &Piidhep, "Piidhep/F");
  Pi -> Branch("Lpidhep", &Lpidhep, "Lpidhep/F");
  Pi -> Branch("Lmidhep", &Lmidhep, "Lmidhep/F");
  Pi -> Branch("Lpmuid", &Lpmuid, "Lpmuid/F");
  Pi -> Branch("Lmmuid", &Lmmuid, "Lmmuid/F");
  Pi -> Branch("Lpeid", &Lpeid, "Lpeid/F");
  Pi -> Branch("Lmeid", &Lmeid, "Lmeid/F");

  for(size_t irow=0 ; irow < in_chain1.GetEntries() ; ++irow){
    in_chain1.GetEntry(irow);
    if (Lpmuid > 0.8 && Lmmuid > 0.8 && Deltam <= 0.2 && Didhep == 421 && Lmidhep == 13 && Lpidhep == -13 && Piidhep == 111){
      Mu -> Fill();
    }
   }

  for(size_t irow=0 ; irow <  in_chain2.GetEntries() ; ++irow){
    in_chain2.GetEntry(irow);
    if (Lpmuid > 0.8 && Lmmuid > 0.8 && Deltam <= 0.2 && Didhep == 421 && Lmidhep == 11 && Lpidhep == -11 && Piidhep == 111){
      E -> Fill();
    }
   }

  for(size_t irow=0 ; irow < in_chain3.GetEntries() ; ++irow){
    in_chain3.GetEntry(irow);
    if (Lpmuid > 0.8 && Lmmuid > 0.8 && Deltam <= 0.2 && Didhep == 421 && Lmidhep == -211 && Lpidhep == 211 && Piidhep == 111){
      Pi -> Fill();
    }
  }

  Mu -> Write();
  E -> Write();
  Pi -> Write();
  ofile -> Close();
}
