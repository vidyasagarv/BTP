import ROOT

# in order to start TMVA
ROOT.TMVA.Tools.Instance()

# open input file, get trees, create output file
input = ROOT.TFile("allmom.root")
tree_s = input.Get("Mu")
tree_ba = input.Get("E")
tree_bb = input.Get("K")
tree_bc = input.Get("Pi")
fout = ROOT.TFile("BDT_multi.root","RECREATE")

# define factory with options
factory = ROOT.TMVA.Factory("TMVAClassification", fout,
                            ":".join(["!V",
                                      "!Silent",
                                      "Color",
                                      "DrawProgressBar",
                                      "AnalysisType=Classification"]
                                     ))

# add discriminating variables for training
factory.AddVariable("ecledivp", 'F')
factory.AddVariable("ecle9e25", 'F')
factory.AddVariable("eclncr", 'F')
factory.AddVariable("Idtof", 'F')
factory.AddVariable("Idacc", 'F')
factory.AddVariable("dedxdive", 'F')
factory.AddVariable("Klmlm", 'F')
factory.AddVariable("Klmlk", 'F')
factory.AddVariable("Klmlp", 'F')
factory.AddVariable("Klmoc", 'F')
factory.AddVariable("Klmnh", 'F')
factory.AddSpectator("mom",'F')
factory.AddSpectator("muid",'F')
factory.AddSpectator("tag",'F')

signalWeight = 1.0
backgroundWeight = 1.0

# define signal and background trees
factory.AddTree(tree_s, "Signal")
factory.AddTree(tree_ba, "bg0")
factory.AddTree(tree_bb, "bg1")
factory.AddTree(tree_bc, "Background")

# define additional cuts
sigCut = ROOT.TCut("")
bgCut = ROOT.TCut("")

# set options for trainings
factory.PrepareTrainingAndTestTree(sigCut,
                                   bgCut,
                                   ":".join(["SplitMode=Random",
                                             "NormMode=NumEvents",
                                             "!V"
                                   ]))

'''
method = factory.BookMethod(ROOT.TMVA.Types.kBDT, "BDT",
                            ":".join(["!H",
                                      "!V",
                                      "NTrees=850",
                                      "MinNodeSize=2.5%",
                                      "MaxDepth=3",
                                      "BoostType=AdaBoost",
                                      "AdaBoostBeta=0.5",
                                      "UseBaggedBoost",
                                      "BaggedSampleFraction=0.5",
                                      "SeparationType=GiniIndex",
                                      "nCuts=20",
                                      ]))
'''

method = factory.BookMethod(ROOT.TMVA.Types.kBDT, "BDTG", "!H:!V:NTrees=1000:BoostType=Grad:Shrinkage=0.10:UseBaggedBoost:BaggedSampleFraction=0.50:nCuts=20:MaxDepth=2");

# self-explaining
factory.TrainAllMethods()
factory.TestAllMethods()
factory.EvaluateAllMethods()

# Saves the output file
fout.Close()

# open the GUI for the result macros
#gui = ROOT.TMVA.TMVAGui("BDT_py.root");

# keep the ROOT thread running
#ROOT.gApplication.Run()

raw_input('Press <ret> to end -> ')
