void createMom(float min_mom = 0.0, float max_mom = 60.0){

  TChain in_chain1("h1");
  TChain in_chain2("h2");
  TChain in_chain3("h3");
  in_chain1.Add("Merged_mu.root/h1");
  in_chain2.Add("Merged_e.root/h2");
  in_chain3.Add("Merged_mu.root/h4");

  TFile* ofile = new TFile("allmom.root","RECREATE");
  TTree* Mu = new TTree("Mu", "Mu Data");
  TTree* E = new TTree("E", "E Data");
  TTree* K = new TTree("K", "K Data");
  TTree* Pi = new TTree("Pi", "Pi Data");


  float mu_sel = 0,e_sel = 0, k_sel = 0, pi_sel = 0;
  float mu_tag = 0,e_tag = 1, k_tag = 2, pi_tag = 3;

  float MuID,
    MuFlag,
    Muchargev,
    Mumomv,
    Mucostv,
    Mudedxv,
    Mudedxev,
    Mudedxdivev,
    MuIdtofv,
    MuIdaccv,
    MuKlmlmv,
    MuKlmlpv,
    MuKlmlkv,
    MuKlmocv,
    MuKlmnhv,
    Muecle9e25,
    Mueclwidth,
    Mueclncr,
    Mueclseed,
    Mueclten,
    Muecltmch,
    Muecltpo,
    Muecltaz,
    Muecltr,
    Musmclsten,
    Muecledivp,
    Musncr,
    Musdmx,
    Musdmx_ncr,
    Musdmx_e9,
    Musdmx_wdth,
    Munclst,
    Muclmx_en,
    Muclmx_mch,
    Mumuid;

  float EID,
    EFlag,
    Echargev,
    Emomv,
    Ecostv,
    Ededxv,
    Ededxev,
    Ededxdivev,
    EIdtofv,
    EIdaccv,
    EKlmlmv,
    EKlmlpv,
    EKlmlkv,
    EKlmocv,
    EKlmnhv,
    Eecle9e25,
    Eeclwidth,
    Eeclncr,
    Eeclseed,
    Eeclten,
    Eecltmch,
    Eecltpo,
    Eecltaz,
    Eecltr,
    Esmclsten,
    Eecledivp,
    Esncr,
    Esdmx,
    Esdmx_ncr,
    Esdmx_e9,
    Esdmx_wdth,
    Enclst,
    Eclmx_en,
    Eclmx_mch,
    Emuid;


  float KID,
    KFlag,
    Kchargev,
    Kmomv,
    Kcostv,
    Kdedxv,
    Kdedxev,
    Kdedxdivev,
    KIdtofv,
    KIdaccv,
    KKlmlmv,
    KKlmlpv,
    KKlmlkv,
    KKlmocv,
    KKlmnhv,
    Kecle9e25,
    Keclwidth,
    Keclncr,
    Keclseed,
    Keclten,
    Kecltmch,
    Kecltpo,
    Kecltaz,
    Kecltr,
    Ksmclsten,
    Kecledivp,
    Ksncr,
    Ksdmx,
    Ksdmx_ncr,
    Ksdmx_e9,
    Ksdmx_wdth,
    Knclst,
    Kclmx_en,
    Kclmx_mch,
    Kmuid;


  in_chain1.SetBranchAddress("idhep",&MuID);
  in_chain1.SetBranchAddress("isthep",&MuFlag);
  in_chain1.SetBranchAddress("charge",&Muchargev);
  in_chain1.SetBranchAddress("mom",&Mumomv);
  in_chain1.SetBranchAddress("cost",&Mucostv);
  in_chain1.SetBranchAddress("dedx",&Mudedxv);
  in_chain1.SetBranchAddress("dedxe",&Mudedxev);
  in_chain1.SetBranchAddress("Idtof",&MuIdtofv);
  in_chain1.SetBranchAddress("Idacc",&MuIdaccv);
  in_chain1.SetBranchAddress("Klmlm",&MuKlmlmv);
  in_chain1.SetBranchAddress("Klmlk",&MuKlmlkv);
  in_chain1.SetBranchAddress("Klmlp",&MuKlmlpv);
  in_chain1.SetBranchAddress("Klmoc",&MuKlmocv);
  in_chain1.SetBranchAddress("Klmnh",&MuKlmnhv);
  in_chain1.SetBranchAddress("ecle9e25",&Muecle9e25);
  in_chain1.SetBranchAddress("eclwidth",&Mueclwidth);
  in_chain1.SetBranchAddress("eclncr",&Mueclncr);
  in_chain1.SetBranchAddress("eclseed",&Mueclseed);
  in_chain1.SetBranchAddress("eclten",&Mueclten);
  in_chain1.SetBranchAddress("ecltmch",&Muecltmch);
  in_chain1.SetBranchAddress("ecltpo",&Muecltpo);
  in_chain1.SetBranchAddress("ecltaz",&Muecltaz);
  in_chain1.SetBranchAddress("ecltr",&Muecltr);
  in_chain1.SetBranchAddress("smclsten",&Musmclsten);
  in_chain1.SetBranchAddress("sncr",&Musncr);
  in_chain1.SetBranchAddress("sdmx",&Musdmx);
  in_chain1.SetBranchAddress("sdmx_ncr",&Musdmx_ncr);
  in_chain1.SetBranchAddress("sdmx_e9",&Musdmx_e9);
  in_chain1.SetBranchAddress("sdmx_wdt",&Musdmx_wdth);
  in_chain1.SetBranchAddress("nclst",&Munclst);
  in_chain1.SetBranchAddress("clmx_en",&Muclmx_en);
  in_chain1.SetBranchAddress("clmx_mch",&Muclmx_mch);
  in_chain1.SetBranchAddress("mu_l",&Mumuid);

  in_chain2.SetBranchAddress("idhep",&EID);
  in_chain2.SetBranchAddress("isthep",&EFlag);
  in_chain2.SetBranchAddress("charge",&Echargev);
  in_chain2.SetBranchAddress("mom",&Emomv);
  in_chain2.SetBranchAddress("cost",&Ecostv);
  in_chain2.SetBranchAddress("dedx",&Ededxv);
  in_chain2.SetBranchAddress("dedxe",&Ededxev);
  in_chain2.SetBranchAddress("Idtof",&EIdtofv);
  in_chain2.SetBranchAddress("Idacc",&EIdaccv);
  in_chain2.SetBranchAddress("Klmlm",&EKlmlmv);
  in_chain2.SetBranchAddress("Klmlk",&EKlmlkv);
  in_chain2.SetBranchAddress("Klmlp",&EKlmlpv);
  in_chain2.SetBranchAddress("Klmoc",&EKlmocv);
  in_chain2.SetBranchAddress("Klmnh",&EKlmnhv);
  in_chain2.SetBranchAddress("ecle9e25",&Eecle9e25);
  in_chain2.SetBranchAddress("eclwidth",&Eeclwidth);
  in_chain2.SetBranchAddress("eclncr",&Eeclncr);
  in_chain2.SetBranchAddress("eclseed",&Eeclseed);
  in_chain2.SetBranchAddress("eclten",&Eeclten);
  in_chain2.SetBranchAddress("ecltmch",&Eecltmch);
  in_chain2.SetBranchAddress("ecltpo",&Eecltpo);
  in_chain2.SetBranchAddress("ecltaz",&Eecltaz);
  in_chain2.SetBranchAddress("ecltr",&Eecltr);
  in_chain2.SetBranchAddress("smclsten",&Esmclsten);
  in_chain2.SetBranchAddress("sncr",&Esncr);
  in_chain2.SetBranchAddress("sdmx",&Esdmx);
  in_chain2.SetBranchAddress("sdmx_ncr",&Esdmx_ncr);
  in_chain2.SetBranchAddress("sdmx_e9",&Esdmx_e9);
  in_chain2.SetBranchAddress("sdmx_wdt",&Esdmx_wdth);
  in_chain2.SetBranchAddress("nclst",&Enclst);
  in_chain2.SetBranchAddress("clmx_en",&Eclmx_en);
  in_chain2.SetBranchAddress("clmx_mch",&Eclmx_mch);
  in_chain2.SetBranchAddress("mu_l",&Emuid);

  in_chain3.SetBranchAddress("idhep",&KID);
  in_chain3.SetBranchAddress("isthep",&KFlag);
  in_chain3.SetBranchAddress("charge",&Kchargev);
  in_chain3.SetBranchAddress("mom",&Kmomv);
  in_chain3.SetBranchAddress("cost",&Kcostv);
  in_chain3.SetBranchAddress("dedx",&Kdedxv);
  in_chain3.SetBranchAddress("dedxe",&Kdedxev);
  in_chain3.SetBranchAddress("Idtof",&KIdtofv);
  in_chain3.SetBranchAddress("Idacc",&KIdaccv);
  in_chain3.SetBranchAddress("Klmlm",&KKlmlmv);
  in_chain3.SetBranchAddress("Klmlk",&KKlmlkv);
  in_chain3.SetBranchAddress("Klmlp",&KKlmlpv);
  in_chain3.SetBranchAddress("Klmoc",&KKlmocv);
  in_chain3.SetBranchAddress("Klmnh",&KKlmnhv);
  in_chain3.SetBranchAddress("ecle9e25",&Kecle9e25);
  in_chain3.SetBranchAddress("eclwidth",&Keclwidth);
  in_chain3.SetBranchAddress("eclncr",&Keclncr);
  in_chain3.SetBranchAddress("eclseed",&Keclseed);
  in_chain3.SetBranchAddress("eclten",&Keclten);
  in_chain3.SetBranchAddress("ecltmch",&Kecltmch);
  in_chain3.SetBranchAddress("ecltpo",&Kecltpo);
  in_chain3.SetBranchAddress("ecltaz",&Kecltaz);
  in_chain3.SetBranchAddress("ecltr",&Kecltr);
  in_chain3.SetBranchAddress("smclsten",&Ksmclsten);
  in_chain3.SetBranchAddress("sncr",&Ksncr);
  in_chain3.SetBranchAddress("sdmx",&Ksdmx);
  in_chain3.SetBranchAddress("sdmx_ncr",&Ksdmx_ncr);
  in_chain3.SetBranchAddress("sdmx_e9",&Ksdmx_e9);
  in_chain3.SetBranchAddress("sdmx_wdt",&Ksdmx_wdth);
  in_chain3.SetBranchAddress("nclst",&Knclst);
  in_chain3.SetBranchAddress("clmx_en",&Kclmx_en);
  in_chain3.SetBranchAddress("clmx_mch",&Kclmx_mch);
  in_chain3.SetBranchAddress("mu_l",&Kmuid);


  Mu -> Branch("ID", &MuID, "ID/F");
  Mu -> Branch("Flag", &MuFlag, "Flag/F");
  Mu -> Branch("charge", &Muchargev, "charge/F");
  Mu -> Branch("mom", &Mumomv, "mom/F");
  Mu -> Branch("cost", &Mucostv, "cost/F");
  Mu -> Branch("dedx", &Mudedxv, "dedx/F");
  Mu -> Branch("dedxe", &Mudedxev, "dedxe/F");
  Mu -> Branch("dedxdive", &Mudedxdivev, "dedxdive/F");
  Mu -> Branch("Idtof", &MuIdtofv, "Idtof/F");
  Mu -> Branch("Idacc", &MuIdaccv, "idacc/F");
  Mu -> Branch("Klmlm", &MuKlmlmv, "Klmlm/F");
  Mu -> Branch("Klmlp", &MuKlmlpv, "Klmlp/F");
  Mu -> Branch("Klmlk", &MuKlmlkv, "Klmlk/F");
  Mu -> Branch("Klmoc", &MuKlmocv, "Klmoc/F");
  Mu -> Branch("Klmnh", &MuKlmnhv, "Klmnh/F");
  Mu -> Branch("ecle9e25", &Muecle9e25, "ecle9e25/F");
  Mu -> Branch("eclwIdth", &Mueclwidth, "eclwidth/F");
  Mu -> Branch("eclncr", &Mueclncr, "eclncr/F");
  Mu -> Branch("eclseed", &Mueclseed, "eclseed/F");
  Mu -> Branch("eclten", &Mueclten, "eclten/F");
  Mu -> Branch("ecltmch", &Muecltmch, "ecltmch/F");
  Mu -> Branch("ecltpo", &Muecltpo, "ecltpo/F");
  Mu -> Branch("ecltaz", &Muecltaz, "ecltaz/F");
  Mu -> Branch("ecltr", &Muecltr, "ecltr/F");
  Mu -> Branch("smclsten", &Musmclsten, "smclsten/F");
  Mu -> Branch("ecledivp", &Muecledivp, "ecledivp/F");
  Mu -> Branch("sncr", &Musncr, "sncr/F");
  Mu -> Branch("sdmx", &Musdmx, "sdmx/F");
  Mu -> Branch("sdmx_ncr", &Musdmx_ncr, "sdmx_ncr/F");
  Mu -> Branch("sdmx_e9", &Musdmx_e9, "sdmx_e9/F");
  Mu -> Branch("sdmx_wdth", &Musdmx_wdth, "sdmx_wdth/F");
  Mu -> Branch("nclst", &Munclst, "nclst/F");
  Mu -> Branch("clmx_en", &Muclmx_en, "clmx_en/F");
  Mu -> Branch("clmx_mch", &Muclmx_mch, "clmx_mch/F");
  Mu -> Branch("muid", &Mumuid, "muid/F");
  Mu -> Branch("tag", &mu_tag, "tag/F");

  E -> Branch("ID", &EID, "ID/F");
  E -> Branch("Flag", &EFlag, "Flag/F");
  E -> Branch("charge", &Echargev, "charge/F");
  E -> Branch("mom", &Emomv, "mom/F");
  E -> Branch("cost", &Ecostv, "cost/F");
  E -> Branch("dedx", &Ededxv, "dedx/F");
  E -> Branch("dedxe", &Ededxev, "dedxe/F");
  E -> Branch("dedxdive", &Ededxdivev, "dedxdive/F");
  E -> Branch("Idtof", &EIdtofv, "Idtof/F");
  E -> Branch("Idacc", &EIdaccv, "idacc/F");
  E -> Branch("Klmlm", &EKlmlmv, "Klmlm/F");
  E -> Branch("Klmlp", &EKlmlpv, "Klmlp/F");
  E -> Branch("Klmlk", &EKlmlkv, "Klmlk/F");
  E -> Branch("Klmoc", &EKlmocv, "Klmoc/F");
  E -> Branch("Klmnh", &EKlmnhv, "Klmnh/F");
  E -> Branch("ecle9e25", &Eecle9e25, "ecle9e25/F");
  E -> Branch("eclwIdth", &Eeclwidth, "eclwidth/F");
  E -> Branch("eclncr", &Eeclncr, "eclncr/F");
  E -> Branch("eclseed", &Eeclseed, "eclseed/F");
  E -> Branch("eclten", &Eeclten, "eclten/F");
  E -> Branch("ecltmch", &Eecltmch, "ecltmch/F");
  E -> Branch("ecltpo", &Eecltpo, "ecltpo/F");
  E -> Branch("ecltaz", &Eecltaz, "ecltaz/F");
  E -> Branch("ecltr", &Eecltr, "ecltr/F");
  E -> Branch("smclsten", &Esmclsten, "smclsten/F");
  E -> Branch("ecledivp", &Eecledivp, "ecledivp/F");
  E -> Branch("sncr", &Esncr, "sncr/F");
  E -> Branch("sdmx", &Esdmx, "sdmx/F");
  E -> Branch("sdmx_ncr", &Esdmx_ncr, "sdmx_ncr/F");
  E -> Branch("sdmx_e9", &Esdmx_e9, "sdmx_e9/F");
  E -> Branch("sdmx_wdth", &Esdmx_wdth, "sdmx_wdth/F");
  E -> Branch("nclst", &Enclst, "nclst/F");
  E -> Branch("clmx_en", &Eclmx_en, "clmx_en/F");
  E -> Branch("clmx_mch", &Eclmx_mch, "clmx_mch/F");
  E -> Branch("muid", &Emuid, "muid/F");
  E -> Branch("tag", &e_tag, "tag/F");

  K -> Branch("ID", &KID, "ID/F");
  K -> Branch("Flag", &KFlag, "Flag/F");
  K -> Branch("charge", &Kchargev, "charge/F");
  K -> Branch("mom", &Kmomv, "mom/F");
  K -> Branch("cost", &Kcostv, "cost/F");
  K -> Branch("dedx", &Kdedxv, "dedx/F");
  K -> Branch("dedxe", &Kdedxev, "dedxe/F");
  K -> Branch("dedxdive", &Kdedxdivev, "dedxdive/F");
  K -> Branch("Idtof", &KIdtofv, "Idtof/F");
  K -> Branch("Idacc", &KIdaccv, "idacc/F");
  K -> Branch("Klmlm", &KKlmlmv, "Klmlm/F");
  K -> Branch("Klmlp", &KKlmlpv, "Klmlp/F");
  K -> Branch("Klmlk", &KKlmlkv, "Klmlk/F");
  K -> Branch("Klmoc", &KKlmocv, "Klmoc/F");
  K -> Branch("Klmnh", &KKlmnhv, "Klmnh/F");
  K -> Branch("ecle9e25", &Kecle9e25, "ecle9e25/F");
  K -> Branch("eclwIdth", &Keclwidth, "eclwidth/F");
  K -> Branch("eclncr", &Keclncr, "eclncr/F");
  K -> Branch("eclseed", &Keclseed, "eclseed/F");
  K -> Branch("eclten", &Keclten, "eclten/F");
  K -> Branch("ecltmch", &Kecltmch, "ecltmch/F");
  K -> Branch("ecltpo", &Kecltpo, "ecltpo/F");
  K -> Branch("ecltaz", &Kecltaz, "ecltaz/F");
  K -> Branch("ecltr", &Kecltr, "ecltr/F");
  K -> Branch("smclsten", &Ksmclsten, "smclsten/F");
  K -> Branch("ecledivp", &Kecledivp, "ecledivp/F");
  K -> Branch("sncr", &Ksncr, "sncr/F");
  K -> Branch("sdmx", &Ksdmx, "sdmx/F");
  K -> Branch("sdmx_ncr", &Ksdmx_ncr, "sdmx_ncr/F");
  K -> Branch("sdmx_e9", &Ksdmx_e9, "sdmx_e9/F");
  K -> Branch("sdmx_wdth", &Ksdmx_wdth, "sdmx_wdth/F");
  K -> Branch("nclst", &Knclst, "nclst/F");
  K -> Branch("clmx_en", &Kclmx_en, "clmx_en/F");
  K -> Branch("clmx_mch", &Kclmx_mch, "clmx_mch/F");
  K -> Branch("muid", &Kmuid, "muid/F");
  K -> Branch("tag", &k_tag, "tag/F");

  Pi -> Branch("ID", &MuID, "ID/F");
  Pi -> Branch("Flag", &MuFlag, "Flag/F");
  Pi -> Branch("charge", &Muchargev, "charge/F");
  Pi -> Branch("mom", &Mumomv, "mom/F");
  Pi -> Branch("cost", &Mucostv, "cost/F");
  Pi -> Branch("dedx", &Mudedxv, "dedx/F");
  Pi -> Branch("dedxe", &Mudedxev, "dedxe/F");
  Pi -> Branch("dedxdive", &Mudedxdivev, "dedxdive/F");
  Pi -> Branch("Idtof", &MuIdtofv, "Idtof/F");
  Pi -> Branch("Idacc", &MuIdaccv, "idacc/F");
  Pi -> Branch("Klmlm", &MuKlmlmv, "Klmlm/F");
  Pi -> Branch("Klmlp", &MuKlmlpv, "Klmlp/F");
  Pi -> Branch("Klmlk", &MuKlmlkv, "Klmlk/F");
  Pi -> Branch("Klmoc", &MuKlmocv, "Klmoc/F");
  Pi -> Branch("Klmnh", &MuKlmnhv, "Klmnh/F");
  Pi -> Branch("ecle9e25", &Muecle9e25, "ecle9e25/F");
  Pi -> Branch("eclwIdth", &Mueclwidth, "eclwidth/F");
  Pi -> Branch("eclncr", &Mueclncr, "eclncr/F");
  Pi -> Branch("eclseed", &Mueclseed, "eclseed/F");
  Pi -> Branch("eclten", &Mueclten, "eclten/F");
  Pi -> Branch("ecltmch", &Muecltmch, "ecltmch/F");
  Pi -> Branch("ecltpo", &Muecltpo, "ecltpo/F");
  Pi -> Branch("ecltaz", &Muecltaz, "ecltaz/F");
  Pi -> Branch("ecltr", &Muecltr, "ecltr/F");
  Pi -> Branch("smclsten", &Musmclsten, "smclsten/F");
  Pi -> Branch("ecledivp", &Muecledivp, "ecledivp/F");
  Pi -> Branch("sncr", &Musncr, "sncr/F");
  Pi -> Branch("sdmx", &Musdmx, "sdmx/F");
  Pi -> Branch("sdmx_ncr", &Musdmx_ncr, "sdmx_ncr/F");
  Pi -> Branch("sdmx_e9", &Musdmx_e9, "sdmx_e9/F");
  Pi -> Branch("sdmx_wdth", &Musdmx_wdth, "sdmx_wdth/F");
  Pi -> Branch("nclst", &Munclst, "nclst/F");
  Pi -> Branch("clmx_en", &Muclmx_en, "clmx_en/F");
  Pi -> Branch("clmx_mch", &Muclmx_mch, "clmx_mch/F");
  Pi -> Branch("muid", &Mumuid, "muid/F");
  Pi -> Branch("tag", &pi_tag, "tag/F");

  for(size_t irow=0 ; irow < in_chain1.GetEntries() ; ++irow){
    in_chain1.GetEntry(irow);
    Mudedxdivev = Mudedxv / Mudedxev;
    Muecledivp = Mueclten/Mumomv;
    if (MuID == -13 && Mumomv > min_mom && Mumomv < max_mom && Mudedxv != 0 && Mudedxev != 0 && MuIdtofv != -1 && MuIdaccv != -1 && Mueclten != 0 && MuKlmlmv != -1.0 && MuKlmlpv != -1.0 && MuKlmlkv != -1.0 && MuKlmocv != -1.0 && MuKlmnhv != -1.0 ){
      Mu -> Fill();
      mu_sel += 1;
    }
    if (MuID == 211 && Mumomv > min_mom && Mumomv < max_mom && Mudedxv != 0 && Mudedxev != 0 && MuIdtofv != -1 && MuIdaccv != -1 && Mueclten != 0 && MuKlmlmv != -1.0 && MuKlmlpv != -1.0 && MuKlmlkv != -1.0 && MuKlmocv != -1.0 && MuKlmnhv != -1.0 ){
      Pi -> Fill();
      pi_sel += 1;
    }
  }

  for(size_t irow=0 ; irow <  in_chain2.GetEntries() ; ++irow){
    in_chain2.GetEntry(irow);
    Ededxdivev = Ededxv / Ededxev;
    Eecledivp = Eeclten/Emomv;
    if (EID == -11 && Emomv > min_mom && Emomv < max_mom && Ededxv != 0 && Ededxev != 0 && EIdtofv != -1 && EIdaccv != -1 && Eeclten != 0 && EKlmlmv != -1.0 && EKlmlpv != -1.0 && EKlmlkv != -1.0 && EKlmocv != -1.0 && EKlmnhv != -1.0 ){
      E -> Fill();
      e_sel += 1;
    }
  }

  for(size_t irow=0 ; irow <  in_chain3.GetEntries() ; ++irow){
    in_chain3.GetEntry(irow);
    Kdedxdivev = Kdedxv / Kdedxev;
    Kecledivp = Keclten/Kmomv;
    if (KID == -321 && Kmomv > min_mom && Kmomv < max_mom && Kdedxv != 0 && Kdedxev != 0 && KIdtofv != -1 && KIdaccv != -1 && Keclten != 0 && KKlmlmv != -1.0 && KKlmlpv != -1.0 && KKlmlkv != -1.0 && KKlmocv != -1.0 && KKlmnhv != -1.0 ){
      K -> Fill();
      k_sel += 1;
    }
  }

  std::cout << "Selected Muons:" << mu_sel << '\n';
  std::cout << "Selected Electrons:" << e_sel << '\n';
  std::cout << "Selected Kaons:" << k_sel << '\n';
  std::cout << "Selected Pions:" << pi_sel << '\n';
  Mu -> Write();
  E -> Write();
  K -> Write();
  Pi -> Write();
  ofile -> Close();

}
// if (Mueclev != -1.0 && Mue9e25v != -1.0 && Mueclnhv != -1.0 && MuKlmlmv != -1.0 && MuKlmlpv != -1.0 && Muklmlkv != -1.0 && Muklmcqv != -1.0 && Muklmocv != -1.0 && Muklmnhv != -1.0 && Mudedxev != 0.0){
