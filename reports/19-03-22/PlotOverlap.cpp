void PlotOverlap(const char *var_name, int no_bins = 100, float var_min = 0, float var_max = 1){

  TChain in_chain_mu("Mu");
  TChain in_chain_pi("Pi");

  in_chain_mu.Add("./../../analysis/dtopimumu/bkg.root/Mu");
  in_chain_pi.Add("./../../analysis/dtopimumu/bkg.root/Pi");

  float var_value;

  in_chain_mu.SetBranchAddress(var_name, &var_value);
  in_chain_pi.SetBranchAddress(var_name, &var_value);

  TH1F *histo_mu = new TH1F("Mu",
                            var_name,
                            no_bins,
                            //in_chain_mu.GetMinimum(var_name),
                            //in_chain_mu.GetMaximum(var_name));
                            var_min,
                            var_max);
  TH1F *histo_pi = new TH1F("Pi",
                            var_name,
                            no_bins,
                            //in_chain_pi.GetMinimum(var_name),
                            //in_chain_pi.GetMaximum(var_name));
                            var_min,
                            var_max);

  histo_mu -> SetLineColor(kGreen);
  histo_pi -> SetLineColor(kRed);

  histo_mu -> SetLineWidth(3);
  histo_pi -> SetLineWidth(3);

  histo_mu -> SetStats(0);
  histo_pi -> SetStats(0);

  histo_mu -> SetTitle(var_name);
  histo_pi -> SetTitle(var_name);

  for(size_t irow = 0 ; irow < in_chain_mu.GetEntries() ; ++irow){
    in_chain_mu.GetEntry(irow);
    histo_mu -> Fill(var_value);
}
  for(size_t irow = 0 ; irow < in_chain_pi.GetEntries() ; ++irow){
    in_chain_pi.GetEntry(irow);
    histo_pi -> Fill(var_value);
  }

  histo_mu -> Scale(0.03);

  auto c = new TCanvas("Variable Plots");

  //c->SetLogy();

  histo_pi -> Draw();
  histo_mu -> Draw("SAME");

 //  TCanvas *c = new TCanvas("c","stacked hists");
 //  hs->Draw("nostack");
 //  return c;
}
