\documentclass[BTech]{iitmdiss}
\usepackage{times}
\usepackage{t1enc}

\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage[driverfallback=dvipdfm]{hyperref} % hyperlinks for references.
\usepackage{amsmath} % easier math formula, align, subequations \ldots

\usepackage{bm}			% Used to make bold math/equations
%\usepackage[nottoc]{tocbibind}	% To include bibliography/references in TOC.
\usepackage{siunitx}		% Primarily to write degrees : like \ang{30}, $ not required.

\graphicspath{{img/}}	% Put all images in this directory. Avoids clutter.

\begin{document}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Title page

\title{IMPROVEMENTS IN MUON IDENTIFICATION OF THE BELLE DETECTOR}

\author{Vobbilisetti Vidya Sagar\break (EP15B030)}

\date{May 2019}
\department{PHYSICS}

%\nocite{*}
\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Certificate
\certificate

\vspace*{0.5in}

\noindent This is to certify that the thesis titled {\bf IMPROVEMENTS IN MUON IDENTIFICATION OF THE BELLE DETECTOR}, submitted by {\bf Vobbilisetti Vidya Sagar (EP15B030)},
  to the Indian Institute of Technology, Madras, for
the award of the degree of {\bf Bachelor of Technology}, is a bona fide
record of the research work done by him under our supervision.  The
contents of this thesis, in full or in parts, have not been submitted
to any other Institute or University for the award of any degree or
diploma.

\vspace*{1.5in}

\begin{singlespacing}
\hspace*{-0.25in}
\parbox{2.5in}{
\noindent {\bf Prof.~Jim Libby} \\
\noindent Research Guide \\
\noindent Professor \\
\noindent Dept. Of Physics\\
\noindent IIT-Madras, 600036 \\
}
\hspace*{1.0in}
\end{singlespacing}
\vspace*{0.25in}

\noindent Place: Chennai\\
Date: 5th May 2019


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Acknowledgments
\acknowledgements

I would like to express my sincere thanks to Prof. Jim Libby for providing guidance for the last 3 years. None of this would have been possible without him.

Thanks to Prof. Gagan Mohanty, TIFR for introducing me to the Belle community and to muon identification. Dr. Varghese Babu has been very helpful and taught me the analysis procedures, thank you very much. \\

\textit{''If I have seen further, it is by standing on the shoulders of giants.'' - Issac Newton}\\

I am thankful to all the giants whose contributions are essential for this work to happen.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Abstract

\abstract

\noindent KEYWORDS: \hspace*{0.5em} \parbox[t]{4.4in}{$\mu$ID ; Belle Detector;
  Boosted Decision Trees.}

\vspace*{24pt}

\noindent We have explored the possibility of optimizing muon identification based on information from almost all sub-detectors of the Belle experiment. We trained a Boosted decision tree model and observed overall improvement in $\mu$ID efficiency while $e$ and $K$ fake rate remained similar to the original. There is a substantial increase in efficiency at low-momentum, 0\% to 82\% at 0.4 GeV/c. But, this came with increased $\pi$ fake rate. Upon solving which this can play a crucial role in searches for some interesting rare decays.
\pagebreak

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Table of contents etc.

\begin{singlespace}
\tableofcontents
\thispagestyle{empty}

%\listoftables
%\addcontentsline{toc}{chapter}{LIST OF TABLES}
\listoffigures
\addcontentsline{toc}{chapter}{LIST OF FIGURES}
\end{singlespace}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Abbreviations
\abbreviations

\noindent
\begin{tabbing}
xxxxxxxxxxx \= xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx \kill
\textbf{$\mu$ID} \> muon Identification \\
\textbf{SM}      \> Standard Model \\
\textbf{FSP} 	 \> Final State Particles \\
\textbf{LFV}  	 \> Lepton Flavor Violating \\
\textbf{FCNC} 	 \> Flavor-Changing Neutral-Currents \\
\textbf{BDT}  	 \> Boosted Decision trees \\
\textbf{TMVA} 	 \> Toolkit for Multi Variate Analysis \\

\textbf{SVD}  \> Silicon Vertex Detector \\
\textbf{CDC}  \> Central Drift Chamber \\
\textbf{ECL}  \> Electromagnetic Calorimeter \\
\textbf{ACC}  \> Aerogel Cherenkov radiation Counter \\
\textbf{TOF}  \> Time Of Flight \\
\textbf{KLM}  \> K-long and Muon Detector \\
\end{tabbing}

\pagebreak
\clearpage

% The main text will follow from this point so set the page numbering
% to Arabic from here on.
\pagenumbering{arabic}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Introduction.

\chapter{Introduction}

When a particle collision happens at high energies all forms of exotic particles are formed. Most of them have very short lifetimes and immediately decay into more stable and lighter particles. It is these final-state stable particles ($\gamma, e^\pm, \mu^\pm, \pi^\pm, K^\pm$, protons and neutrons) that travel to the detectors. Identifying these final state particles and reconstructing their trajectories allows us to discover and study the high-energy (massive) short-living particles which we cannot detect directly.

Muons are one of the fundamental particles in the Standard Model. Similar to electrons, muons are leptons with charge -1\textit{e} and spin 1/2, but with much greater mass.

Precise muon identification is essential for many studies in experimental particle physics.
Electrons are primarily detected/identified using electromagnetic calorimeters (ECL).
But, due to higher mass muons do not lose as much energy as electrons through Bremsstrahlung radiation (produced by the deceleration of a charged particle when deflected by another charged particle) inside calorimeters and can easily pass through them.
Hence, most experiments devise a dedicated detectors (the KLM subdetector in the case of Belle) outside calorimeters (often as the outer most subdetector layer) to detect muons.

The distance of these dedicated muon detectors from the interaction point dictates a minimum momentum a muon must have to reach there and be detected. In the case of the Belle detector, KLM detector cannot detect muons with momentum less than 500 MeV/c. This limit is not a problem in the case of most processes where muons are of high enough momentum.
But there exists a few rare and interesting decays like Lepton Flavor Violating (LFV) $\tau^+ \rightarrow \mu^+ \mu^- \mu^-$ and Flavor-changing neutral-current (FCNC) $D^0 \rightarrow \pi^0 \mu^+ \mu^-$ with considerable amounts of final state muons with momentum less than 500 MeV/c (we found it to be 18\% of muons in the case of LFV $\tau$ decay using Monte Carlo Simulation).

In this project, we explored the potential improvements in muon identification by including information from the subdetector layers closer to interaction point than the muon detector.
Since distinguishing muons from photons, protons and neutrons is quite trivial, we focused on discriminating efficiency of muons from electrons, pions and kaons.
We trained a classifier based on information from different layers of subdetectors and observed a considerable improvement in muon identification efficiency, primarily in the low-momentum region.

We have also attempted to search for FCNC decay $D^0 \rightarrow \pi^0 \mu^+ \mu^-$ using this improved muon identification.

\chapter{The Belle detector}

Located in Japan, KEKB is an electron-positron asymmetric energy accelerator which collides 8.0 GeV $e^-$ with 3.5 GeV $e^+$ primarily at the $\Upsilon$(4S) resonance i.e., 10.58 GeV center of mass energy.
The Belle Detector surrounds KEKB beam crossing/interaction point.
During its runtime between 1999 and 2010, KEKB broke all the records for both integrated and instantaneous luminosity of a high energy accelerator leading to Belle being able to collect over 1 ab$^{-1}$ of data.
The primary aim for designing Belle was the observation of CP violation in the B meson systems. Along with the BaBar experiment in Stanford, Belle was able to observe large \textit{CP} asymmetries in \textit{B}-meson decays, in 2001, fulfilling its purpose. The 2008 Physics Nobel Prize citation explicitly recognized this experimental result.

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{side-view-of-belle}
	\caption{Side view of the Belle detector}
	\label{fig:side-belle}
\end{figure}

The Belle detector is a $4\pi$ solid-angle spectrometer surrounded by a 1.5 Tesla superconducting solenoid magnet.
It is composed of:
\begin{itemize}
	\item Layers of \textbf{Silicon Vertex Detector (SVD)} to detect \textit{B}-meson decay vertices and perform low-momentum tracking;
	\item Layers of \textbf{Central Drift Chamber (CDC)} to track trajectories of charged particles and also determine their momenta;
	\item An \textbf{electromagnetic calorimeter (ECL)} based on CsI(Tl) to detect photons and electrons, and determine their direction and energy;
	\item An array of silica \textbf{Aerogel Cherenkov counters (ACC)} and an arrangement of \textbf{time-of-flight counters (TOF)} to identify charged hadrons;
	\item  Layers of iron plates interleaved with resistive plate chambers outside the solenoid to detect \bm{$K^0_L$} \textbf{mesons and muons (KLM)}.
\end{itemize}

More detailed information on each subdetector is available in the Reference [\cite{belletdr}].

\section{Muon Identification}
\label{sec:muid}

The KLM detector was designed for identification of $K_L$'s and muons efficiently over the momentum range $>$ 600 MeV/c.
It has two parts: The barrel shaped component around the interaction point, Barrel-KLM, covering an angular range of \ang{45} to \ang{125} polar angles. And the Endcap-KLM extending this range to \ang{20} and \ang{155} [\cite{belleklm}].

KLM is made of alternating layers of charged particle detectors (RPCs) and iron plates.
The barrel region contains 15 detector layers and 14 iron layers while each of the forward and the backward end-caps have 14 detector layers.
These iron layers provide 3.9 interaction lengths of material in total for any particle traveling perpendicular to the plane of detectors.
The discrimination between muons and charged hadrons ($\pi^\pm$ or $K^\pm$) is done based upon their range and transverse scattering in the multiple layers of charged particle detectors and iron. On average, muons deflect less and travel much farther.

The charged particle detectors used here are glass-electrode resistive plate counters (RPCs) [\cite{bellerpc}].
Resistive plate counters are made of parallel plate electrodes with very high bulk resistivity ($\geq 10^{10} \Omega$ cm) separated by a gap filled by a gas.
When an ionizing particle travels through this gap, it initiates a streamer in the gas resulting in a local discharge of the electrodes. Limitations on this discharge is controlled by the resistivity of the electrodes and the quenching properties of the gas. The external pickup strips gets induced by this discharge which record the location and also the time of ionization.

The detection efficiency and resolution of these superlayers was measure using cosmic-rays. The detectors module's response to penetrating muons was measured and these results were used for detector simulation.

\begin{figure}[h]
	\centering
	\includegraphics{muon-detection-eff-vs-mom}
	\caption{Muon detection efficiency vs. Momentum in KLM}
	\label{fig:klm-mu-eff}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics{muon-detection-fake-vs-mom}
	\caption{Muon detection fake rate vs. Momentum in KLM}
	\label{fig:klm-mu-fake}
\end{figure}

The muons with momentum $<$ 500 MeV/c do not reach the KLM detector.
By comparing a particle's measured range (interaction length, hits per strip, strips per layer) with the predicted range for a muon, a likelihood of being a muon is assigned.
The Figure \ref{fig:klm-mu-eff} shows the muon detection efficiency vs. momentum using a cut on likelihood at 0.66.
Inevitably some $\pi^\pm$ and $K^\pm$ will be misidentified as muons.
Using a sample of $K_S \rightarrow \pi^+ \pi^-$ events this fake rate is determined.
The fraction of such misidentified $\pi^\pm$ is shown in Figure \ref{fig:klm-mu-fake}, again with a cut on muon likelihood at 0.66.
Above 1.5 GeV/c momentum we have high identification efficiency ($>$ 90\%) with comparably small fake rate ($<$ 5\%) [\cite{belletdr}].

\chapter{Method}

We initially started by building classifiers using Monte Carlo simulated data of the decay chain:
$$B^0 \rightarrow J/\psi(1S) \ \phi \ K^0_S$$
$$J/\psi(1S) \rightarrow \ell^+ \ell^-, \ \ \ell = e,\mu$$
$$\phi \rightarrow K^+ K^-$$
$$K^0_S \rightarrow \pi^+ \pi^-$$
for two reasons: it all four required final state particles ($\mu$, e, $\pi$ and K) in a single decay; and it profits from sharper mass distributions of involved resonances namely, $J/\psi(1S)$, $\phi$ and $K^0_S$.

But we later switched on to a different decay chain:
$$e^+ e^- \rightarrow vpho \rightarrow c \bar{c}$$
$$D^{*+} \rightarrow D^0 \pi^+$$
$$D^0 \rightarrow K^\pm \ell^\mp \nu_\ell , \ \ \ell = e. \mu$$

since its phase space allows for more low-momentum muons. All the analysis procedure described further is for this decay chain.

\section{ Monte Carlo production and reconstruction}

The EvtGen is a package/framework for simulating physics processes. It is an event generator specialized in simulating decays of B mesons [\cite{evtgen}].
Using amplitudes of each node of a decay tree, EvtGen simulates entire decay chain.
It makes sure that the implementation of amplitude of each decay does not depend on how its mother particle was generated or how its daughter particles are going to decay.

The files containing simulated decay chains are further processed by a GEANT model of the Belle detector. GEANT4 is a toolkit for simulating the detector's response to particles by essentially simulating the passage of particles through matter. It provides geometry, tracking, hits and physics models functionalities [\cite{geant4}]. This simulates the electronic outputs of all the readouts of subdetectors.

The output of GEANT is used for reconstruction similarly as the real data is reconstructed and further used as training and testing data for classification.

\section{Classification}

In the terminology of statistical/machine learning, our problem comes under supervised classification.
According to Tom Mitchell, a modern definition of machine learning is ``A computer program is said to learn from experience E with respect to some class of tasks T and performance measure P, if its performance at tasks in T, as measured by P, improves with experience E.'' [\cite{tomml}]. Or simply, building a model based on known (training) data to predict a quantitative/qualitative result is statistical learning.

Statistical/Machine learning can be broadly categorized as: Supervised and Unsupervised learning. In supervised learning we provide a data set with know results and try to find relationship between the input variables and the output. On other hand, in unsupervised learning there is no intended outcome, the goal is just to describe possible patterns and associations among a set of input variables.

Supervised learning can again be categorized into: regression and classification. In regression we estimate the parameters of a function, which predicts the value of output (could be a vector) in terms of input variables. In a classification, we try to predict results as a discrete output (like a mail being Spam or not-Spam). Or to say, we want to map all the input variables into categories (discrete).

An example for supervised classification is to predict whether a student gets admitted or not into a university using historical data from previous applicants as a training data. For each previous applicant, we have the their scores on 2 different exams (input variables) and their admissions decision (output) as shown in Figure \ref{fig:score-dist}.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{lin1}
	\caption{Distribution of Applicant's exam scores}
	\label{fig:score-dist}
\end{figure}

We want to find a linear decision boundary between both the classes: Admitted and Not admitted. This is done by obtaining logistic regression hypothesis defined as $h_\theta(x) = g(\theta^T x)$ where $x$ is a input vector, $g$ is the sigmoid function $g(z) = \frac{1}{1+e^{-z}}$ and $h_\theta(x)$ represents the probability for a data-point $x$ to belong to class-1 . If $y$ represents the output (class) then $h_\theta(x) = P(y=1 | x ; \theta) = 1 - P(y=0 | x ; \theta)$. If $h_\theta(x) > 0.5$, we consider it as class-1 object (Admitted in our case) or as class-0 object (Not admitted) otherwise. The parameter vector $\theta$ for the hypothesis are chosen to minimize the cost function given by,
$$J(\theta) = \frac{1}{m}\sum_{i = 1}^{m}[-y^{(i)} log(h_\theta(x^{(i)})) - (1-y^{(i)}) log(1-h_\theta(x^{(i)}))]$$
where m is the number of training events.

Using gradient descent to find the optimal parameters of this linear classification model gives the decision boundary as shown in Figure \ref{fig:score-bound}.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{lin2}
	\caption{Predicted decision boundary using linear classification model}
	\label{fig:score-bound}
\end{figure}

This works well as long as the decision boundary can be approximated to be linear. If we have data in non-linear arrangement, one strategy to obtain better fit is by creating more features (input variables to the model) from each data point. For the example data set represented in Figure \ref{fig:nonlinear-bound}, we used all polynomial terms (up to 6th order) of $x_1$ and $x_2$ as input variables.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{nonlin2}
	\caption{Predicted boundary using polynomial terms as inputs}
	\label{fig:nonlinear-bound}
\end{figure}

\subsection{Boosted Decision Trees}

Using just higher order polynomial terms may not be sufficient in all cases. And, if we have too many inputs, the hypothesis/model may fit the training data well, but will fail to extend to new examples (a consequence called overtraining).
Manually selecting which features to use is also not feasible. Neural networks were the popular choice for such nonlinear classification.
But, the ability of Boosted Decision trees to be visualized as simple 2D tree structure provides a straightforward interpretation with nodes representing rectangular cuts in hyperspace makes them more preferred.
We have compared the performance (ROC curves) of both Neural Networks (more common in our field) and Boosted Decision trees on our data set and found them to be comparable.

A decision tree is a binary-tree structured classifier like the one sketched in Figure \ref{fig:bdt-binary-tree}. Left/right decisions are repeatedly taken on a single variable at a time until a defined stop criterion is fulfilled. By this, the total phase space of input variables is partitioned into many regions that are eventually classified based on the what the majority of events end up in that region.
Boosting is an additional step done to enhance the performance (and also stability against possible statistical fluctuations in training data) by sequentially applying the classifier to reweighted variants of the same training sample and finally constructing a weighted average of the individual classifiers thus produced into a single classifier.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{binary-tree}
	\caption{Schematic view of a decision tree. S and B stands for Signal and Background, different classes.}
	\label{fig:bdt-binary-tree}
\end{figure}


\subsection{TMVA}

The Toolkit for Multivariate Analysis (TMVA) is an environment (integrated with ROOT) for the training, testing and evaluating performance of multivariate classification and regression techniques [\cite{tmva}].
TMVA also provides some good visualization scripts. At the end besides method-specific training results, we can run various macros through a graphical interface.

Some of the distributions it displays are: histograms of input variables in both signal and background; correlation coefficients matrix of input variables; classifier distributions (also superimposed test and training samples to observe overtraining); Figure of Merit, efficiencies and purities vs. the classifier cut; ROC curve: background rejection versus signal efficiency from the test sample. It also has some method-specific visualizations like decision trees when using BDT and network architecture when using neural networks.

The input events are divided into training and test samples randomly to maintain statistically independent evaluation. Also, input variables are normalized (linearly scaled to lie within [-1, 1]) in order to provide a direct comparisons between the separation power of input variables in terms of weights assigned to them.

More description on how TMVA implements Boosted Decision Trees can be found in Sec. 8.12 in the Ref. [\cite{tmva}].


\subsection{Variables used}

Inspired by the $\mu$-identification described in the Ref. [\cite{Lattice:2015tia}], we used the following variables to build a BDT classifier:
\begin{itemize}
	\item \bm{$dE/dX/(dE/dX)_{\mu}$}: Ratio of measured energy loss in CDC and expected muon energy loss. The expected mean energy loss is calculated based on the Bethe-Bloch formula for the gas used in CDC. If the particles were true muons, this variable would be close to 1.
	\item \textbf{Likelihood for the muon hypothesis based on ACC measurement}: When a particle travels faster than the velocity of light in that medium, they produce Cherenkov radiations. Based on the location of photons emitted, a likelihood is calculated.
	\item \textbf{Likelihood for the muon hypothesis based on TOF measurement}: TOF measures the time taken by a particle to move from a layer to the other. As different types of particles having the same momentum take different time, it helps in identification.
	\item \bm{$E_{ECL}/p_\mu$}: Ratio of matched cluster energy in ECL to muon candidate’s momentum.
	\item \bm{$E_9/E_{25}$} \textbf{– photon-like and isolation variable}: Ratio of energy deposited in 3x3 grid and 5x5 around the peak in ECL. This is a description of transverse spread of energy deposition in calorimeters.
	\item \textbf{Number of ECL crystals per matched cluster}: Counting how many crystals are hit.
	\item \textbf{Likelihood for the muon hypothesis based on KLM measurement}: By comparing particle's range (interaction length, hits per strip, strips per layer) with that of muons a likelihood is assigned as described in Section \ref{sec:muid}.
	\item \textbf{Likelihood for the pion hypothesis based on KLM measurement}: Similar to muon hypothesis.
	\item \textbf{Likelihood for the kaon hypothesis based on KLM measurement}: Similar to muon hypothesis.
	\item \textbf{Outcome of KLM}: A discrete variable with following possibilities: (0) the candidate didn't reach KLM, (1) it stopped in the Barrel KLM, (2) it stopped in the Endcap KLM, (3) it exited the Barrel KLM, or (4) it exited Endcap KLM.
    \item \bm{$N_{\rm hits}$}: Number of associated two-dimensional hits in KLM. Another measure of deflection while traveling and muons have smaller deflections comparatively.
\end{itemize}

Figures \ref{fig:var1}-\ref{fig:var11} shows the distributions of these variables for the four final state particles of interest.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{var1}
	\caption{Distribution of $dE/dX/(dE/dX)_{\mu}$}
	\label{fig:var1}
\end{figure}
\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{var2}
	\caption{Distribution of muon likelihood based on ACC measurements. Note that the Y-axis is in logarithmic scale}
	\label{fig:var2}
\end{figure}
\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{var3}
	\caption{Distribution of muon likelihood based on TOF measurements. Note that the Y-axis is in logarithmic scale}
	\label{fig:var3}
\end{figure}
\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{var4}
	\caption{Distribution of $E_{ECL}/p_\mu$}
	\label{fig:var4}
\end{figure}
\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{var5}
	\caption{Distribution of $E_9/E_{25}$}
	\label{fig:var5}
\end{figure}
\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{var6}
	\caption{Distribution of number of ECL crystals per matched cluster}
	\label{fig:var6}
\end{figure}
\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{var7}
	\caption{Distribution of muon likelihood based on KLM measurements. Note that the Y-axis is in logarithmic scale}
	\label{fig:var7}
\end{figure}
\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{var8}
	\caption{Distribution of pion likelihood based on KLM measurements. Note that the Y-axis is in logarithmic scale}
	\label{fig:var8}
\end{figure}
\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{var9}
	\caption{Distribution of kaon likelihood based on KLM measurements. Note that the Y-axis is in logarithmic scale}
	\label{fig:var9}
\end{figure}
\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{var10}
	\caption{Distribution of KLM outcome}
	\label{fig:var10}
\end{figure}
\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{var11}
	\caption{Distribution of number of hits in KLM}
	\label{fig:var11}
\end{figure}

Upon training a BDT classifier model using these 11 variables we observed a little increase in efficiency of identifying muons at higher momenta, with a similar electron and kaon fake rates as original. But there is a substantial improvement in muon identification efficiency at low momentum as shown in Figure \ref{fig:improved-muid} accompanied by huge spike in pion fake rate.

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{improved-muid.png}
	\caption{$\mu$ID efficiency and fake rates vs. momentum. Here the error bars denote binomial errors.}
	\label{fig:improved-muid}
\end{figure}

In order to understand and rectify the increased $\pi$ fake rate, we built 3 individual different BDT classifiers in momentum ranges: $p < 0.5 GeV/c$, $0.5 GeV/c \leq p < 1.0GeV/c$ and $1.0 GeV/c \leq p$ and compared. But this did not decrease pion fake rate much. To understand more, we would like to analyzing another decay mode where pions occupy a similar phase space as muons unlike here where pions are produced from $D^{*+}$ decay and muons are from $D^0$ which caused their momentum distributions to be quite different. But, we still do not understand why our $\mu$ID has more $\pi$ fake rate than the original even though we use all the variables the original uses and more.


\chapter{Application: $D^0 \rightarrow \pi^0 \mu^+ \mu^-$}

So far the Standard Model has been consistent with most of the phenomenon in particle physics. But it need not be the ultimate theory. In this situation, searches for rare processes like flavor-changing neutral-currents (FCNC) decays which are highly suppressed in the Standard Model (SM) are particularly interesting because they may point us towards the "new physics" like the superstring models [\cite{Campbell:1986xd}].

The FCNC decays $D^0 \rightarrow \pi^0 \mu^+ \mu^-$ can happen at one-loop level in SM as penguin and box diagrams as shown Figure \ref{fig:app-one-loop}, but are highly suppressed by the GIM mechanism [\cite{Glashow:1970gm}]. The theoretical estimates based on these one loop diagrams for its branching fractions are of the order $10^{-9}$ [\cite{Schwartz:1993he}].

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{fcnc-1.png}
	\caption{Short distance contributions to D meson FCNC decays due to (a) box and (b) penguin diagrams.}
	\label{fig:app-one-loop}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{fcnc-2.png}
	\caption{Long distance contributions to FCNC decays in D mesons due to (a) photon pole amplitude and (b) vector meson dominance.}
\end{figure}

Along with these short distance loop diagrams there are also contributions from long distance effects. There are two such contributions: photon pole amplitudes and vector meson dominance. Both involve nonperturbative QCD factors which are complicated to calculate.


\section{Previous analysis}

FCNC decays of strange quarks, like $K^0_L \rightarrow e^+ e^-$, $K^0_L \rightarrow \mu^+ \mu^-$ and $K^0_L \rightarrow \pi^0 e^+ e^-$ have been found and bounded at the level of $10^{-10}$, near its theoretical estimates [\cite{PDG2018}]. However, in the charm sector it is only in the last few years that the bounds on FCNC decays has improved to that scale. The best experimental bound we have on $D^0 \rightarrow \pi^0 \mu^+ \mu^-$ is still in the order of $10^{-4}$ from 1995-96 studies at Fermilab's E653 and CESR's CLEO II detectors [\cite{Campbell:1986xd, Freyberger:1996it}].

Given the record breaking luminosity of the Belle detector, it is a good place to search for these FCNC rare decays of charm quarks. And we believe that improved $\mu$ID, especially at low-momentum will help attain better bounds on the branching fraction.

\section{$\mu$ID cut optimization}

For the decay mode $D^0 \rightarrow \pi^0 \mu^+ \mu^-$ the primary source of background is $D^0 \rightarrow \pi^0 \pi^+ \pi^-$. And because muons and pions have similar mass, they are the hardest to distinguish as can also be seen in Figure \ref{fig:improved-muid}.

We found the optimum $\mu$ID (the original) cut to distinguish the signal from background to be 0.67 (very close to 0.66 used for generic performance study described in Sec. \ref{sec:muid}) which increased purity from $(2.22\pm 0.02)\%$ to $(91 \pm 3)\%$ as shown in transformation from Figure \ref{fig:app-1} to \ref{fig:app-2}.

We used the standard Figure of Merit: $Signal/\sqrt{Signal+Background}$ to identify this optimal cut.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.9\textwidth]{before-muid-cut}
	\caption{Distribution of $D^0$ mass before applying muID cut}
	\label{fig:app-1}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[ width=0.9\textwidth]{after-muid-cut}
	\caption{Distribution of $D^0$ mass after applying muID cut}
	\label{fig:app-2}
\end{figure}


\chapter{Conclusions and Discussion}

By using information from other subdetectors alongside KLM we observed an overall improvement in muon identification efficiency and substantial increase in low-momentum. For muons with 0.4 GeV/c momentum, the efficiency increased from 0\% to 82\%.  If the increase in pion-fake rate can be understood and rectified then improved $\mu$ID has a lot of potential applications in searches for various rare decays. Implementing similar $\mu$ID in Belle II can take advantage of the intended 40 times larger integrated luminosity as well.

If everything works as hoped I will eventually work on the other potential application of this $\mu$ID, Lepton Flavor violating $\tau^+ \rightarrow \mu^+ \mu^- \mu^+$, for my Ph.D.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Bibliography.

\begin{singlespace}
  \bibliography{refs}
\end{singlespace}

\end{document}
