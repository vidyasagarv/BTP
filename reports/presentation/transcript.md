# Slide 0

Greetings.

I am presenting 2 years' of my attempts to improve muon identification of the Belle Detector.


# Slide 1

When high energy particles are collided in particle accelerators all kinds of exotic short lived particles are formed. And they almost immediately decay into lighter long lived particles. And these final state particles are the ones we can actually detect. Reconstructing them is how we discover and study the exotic particles. The common final state particles detected in particle physics experiments are photons, electrons, muons, charged pions, charged kaons, protons and neutrons.

Just like electrons, muons are also leptons with same charge and spin. But muons are very massive compared to electrons, in fact, 205 times.

The way electrons are usually detected or identified is by electromagnetic calorimeters.

But due to higher mass of muons, they do not easily undergo Bremsstrahlung radiation, which is the EM radiation produced by the deceleration of a charged particle when deflected by another charged particle. Power radiated this way is proportional to $m^{-4}$ or $m^{-6}$ based on the angle between acceleration and velocity. Hence most modern experiments design a dedicated detectors based on Resistive Plate Chambers far from interacting point to detect muons. K_long and muon detector (KLM) is such specialized detector in the case of Belle.

The detector being far from the interacting point means that the particle or the muons needs to have certain momentum to reach the detector. This imposes a lower momentum condition on muons to be detected. (0.6 GeV/c in the case of Belle).


# Slide 2

KEKB is an asymmetric energy particle accelerator in Japan. The Belle detector surrounds its interacting point. So, electrons and positrons of certain energy come this way and collide. Various exotic particle are produced and decay into the final state stable particle mentioned in the previous slide. The Super conducting 1.5 T solenoid deflects the charged final state particle into the subdetectors.

I will give a brief description about each subdetector:
1) The inner most is the Silicon Vertex Detector, used to measure B meson decay vertices and perform low-momentum tracking.
2) Next is the Central Drift Chamber which also tracks particle and used to determine their momentum.
3) After that is the Aerogel Cherenkov counter used primarily for particle identification. When a particle moves at speeds higher than lights velocity in that medium they emit Cherenkov radiation. Size of the Cherenkov cone along with momentum information that is obtained using the first 2 detectors tells the mass of the particle.
4) The next detector, Time of Flight, measures the time taken by the particle to travel between two layers giving us the velocity. Again along with momentum information this can tell the mass of the particle. Mass is kind of a distinguishable property among most particles.
5) Next is Electromagnetic calorimeter to detect photons and electrons as mentioned earlier. It also determines their direction and energy.
6) And finally after the solenoid itself, there is the KLM detector designed to detect K_longs and muons. It consists of layers of iron core and RPCs. Resistive plate counters have two parallel plate electrodes with high bulk resistivity separated by a gas-filled gap. An ionizing particle traversing the gap initiates a streamer in the gas that resulting in a local discharge of the plates. The discharge induces a signal on external pickup strips, which can be used to record the location and the time of the ionization. The multiple layers of charged particle detectors and iron allow the discrimination between muons and charged hadrons ($\pi^\pm$ or $K^\pm$) based upon their range and transverse scattering. Muons travel much farther with smaller deflections on average.

The existing muon identification only uses information from KLM detector which gives efficient discriminating power of muons above certain momentum range. And this is good enough in most of the cases. But there are certain interesting decays with limited phase space where significant amount of final state muons have low momentum. We attempted to improve muon identification, primarily for low muons by including the information from inner layers of subdetectors as well.


# Slide 3

Using Monte Carlo methods, we simulated D^0 -> K+ \mu- \nu_mu decays. And used it as a training data to build a Machine Learning model based on these variables.

We built a Boosted decision tree model using:
1) muon Likelihood based on measurements of KLM, which works as explained in previous slide.
2) pion
3) and kaon likelihood also from KLM,
4) Number of hits in KLM,
5) chi square, which measure the spread of hits in KLM,
6) outcome of KLM: which is a discrete variable with 5 possibilities:
	0: did not reach KLM
	1: reached Barrel KLM and stopped there
	2: stopped in Endcap KLM
	3: exited Barrel KLM
	4: exited Endcap KLM
7) Rate of energy released per distance traveled in Central drift chamber. Comparing with Bethe Bloch diagram, we can identify the particles.
8) muon Likelihood from TOF measurements, which works as mentioned in the previous slide
9) Same from Aerogel Cherenkov counter also
10) Ratio of matched cluster energy in ECL to muon candidate's momentum
11) ECL detector is in the form of pixels. Ratio of energy released in a 3x3 pixels around the peak and 5x5 will tell about the spread of the energy dissipation.
12) Number of total crystals per matched cluster will tell about the overall spread.

In most simple terms, machine learning boils all these variables into a single one, applying a cut (using the Figure of Merit S/sqrt(S+B)) will do the classification. And this is the output of the Boosted Decision Tree we trained.

# Slide 4
# Slide 5
# Slide 6


# Possible Q&A

## Why asymmetric energy?

## Formula for Cherenkov radiation?

cos(\theta) = 1/(\eta\beta)

## Bethe Bloch diagram

## Why BDT instead of Neural networks?

##
