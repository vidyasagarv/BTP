void PlotOverlap(const char *var_name, float var_min = 0, float var_max = 1, int no_bins = 100){

  TChain in_chain_mu("Mu");
  TChain in_chain_pi("Pi");

  in_chain_mu.Add("dtopimumu.root/Mu");
  in_chain_pi.Add("dtopimumu.root/Pi");

  float var_value,lpmuid,lmmuid;

  in_chain_mu.SetBranchAddress(var_name, &var_value);
  in_chain_mu.SetBranchAddress("Lpmuid", &lpmuid);
  in_chain_mu.SetBranchAddress("Lmmuid", &lmmuid);
  in_chain_pi.SetBranchAddress(var_name, &var_value);
  in_chain_pi.SetBranchAddress("Lpmuid", &lpmuid);
  in_chain_pi.SetBranchAddress("Lmmuid", &lmmuid);

  TH1F *histo_mu_full = new TH1F("Mu full",
                            var_name,
                            no_bins,
                            //in_chain_mu.GetMinimum(var_name),
                            //in_chain_mu.GetMaximum(var_name));
                            var_min,
                            var_max);
  TH1F *histo_pi_full = new TH1F("Pi full",
                            var_name,
                            no_bins,
                            //in_chain_pi.GetMinimum(var_name),
                            //in_chain_pi.GetMaximum(var_name));
                            var_min,
                            var_max);

  TH1F *histo_mu = new TH1F("Mu",
                            var_name,
                            no_bins,
                            //in_chain_mu.GetMinimum(var_name),
                            //in_chain_mu.GetMaximum(var_name));
                            var_min,
                            var_max);
  TH1F *histo_pi = new TH1F("Pi",
                            var_name,
                            no_bins,
                            //in_chain_pi.GetMinimum(var_name),
                            //in_chain_pi.GetMaximum(var_name));
                            var_min,
                            var_max);

  histo_mu_full -> SetLineColor(kGreen);
  histo_pi_full -> SetLineColor(kRed);
  histo_mu -> SetLineColor(kGreen);
  histo_pi -> SetLineColor(kRed);

  histo_mu_full -> SetLineWidth(2);
  histo_pi_full -> SetLineWidth(2);
  histo_mu -> SetLineWidth(2);
  histo_pi -> SetLineWidth(2);

  histo_mu_full -> SetStats(0);
  histo_pi_full -> SetStats(0);
  histo_mu -> SetStats(0);
  histo_pi -> SetStats(0);

  histo_mu  -> SetTitle("muons after muID cut");
  histo_pi  -> SetTitle("pions after muID cut");
  histo_mu_full  -> SetTitle("muons before muID cut");
  histo_pi_full  -> SetTitle("pions before muID cut");

  histo_mu -> GetXaxis()-> SetTitle("D0 mass");
  histo_pi -> GetXaxis()-> SetTitle("D0 mass");
  histo_mu_full -> GetXaxis()-> SetTitle("D0 mass");
  histo_pi_full -> GetXaxis()-> SetTitle("D0 mass");

  for(size_t irow = 0 ; irow < in_chain_mu.GetEntries() ; ++irow){
    in_chain_mu.GetEntry(irow);
    histo_mu_full -> Fill(var_value);
    if(lpmuid >= 0.67 && lmmuid >= 0.67){
    	histo_mu -> Fill(var_value);
    }
}
  for(size_t irow = 0 ; irow < in_chain_pi.GetEntries() ; ++irow){
    in_chain_pi.GetEntry(irow);
    histo_pi_full -> Fill(var_value);
    if(lpmuid >= 0.67 && lmmuid >= 0.67){
    	histo_pi -> Fill(var_value);
    }
  }

  //histo_mu_full -> Scale(0.03);
  //histo_mu -> Scale(0.03);

  auto c = new TCanvas("Variable Plots");

  //c->SetLogy();

  //histo_pi_full -> Draw();
  histo_mu -> Draw();
  //histo_mu_full -> Draw("SAME");
  histo_pi -> Draw("SAME");

  gStyle->SetOptTitle(0); // Remoces title bar
 //  TCanvas *c = new TCanvas("c","stacked hists");
 //  hs->Draw("nostack");
 //  return c;
  c->BuildLegend();
}
