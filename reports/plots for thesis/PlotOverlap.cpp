void PlotOverlap(const char *var_name, float var_min = 0, float var_max = 1, const char *titlet, int no_bins = 100){

  TChain in_chain_mu("Mu");
  TChain in_chain_pi("Pi");
  TChain in_chain_e("E");
  TChain in_chain_k("K");

  in_chain_mu.Add("dtokmunu.root/Mu");
  in_chain_pi.Add("dtokmunu.root/Pi");
  in_chain_e.Add("dtokmunu.root/E");
  in_chain_k.Add("dtokmunu.root/K");

  float var_value;

  in_chain_mu.SetBranchAddress(var_name, &var_value);
  in_chain_pi.SetBranchAddress(var_name, &var_value);
  in_chain_e.SetBranchAddress(var_name, &var_value);
  in_chain_k.SetBranchAddress(var_name, &var_value);

  TH1F *histo_mu = new TH1F("Mu",
                            var_name,
                            no_bins,
                            //in_chain_mu.GetMinimum(var_name),
                            //in_chain_mu.GetMaximum(var_name));
                            var_min,
                            var_max);
  TH1F *histo_pi = new TH1F("Pi",
                            var_name,
                            no_bins,
                            //in_chain_pi.GetMinimum(var_name),
                            //in_chain_pi.GetMaximum(var_name));
                            var_min,
                            var_max);
  TH1F *histo_e = new TH1F("E",
                            var_name,
                            no_bins,
                            //in_chain_mu.GetMinimum(var_name),
                            //in_chain_mu.GetMaximum(var_name));
                            var_min,
                            var_max);
  TH1F *histo_k = new TH1F("K",
                            var_name,
                            no_bins,
                            //in_chain_mu.GetMinimum(var_name),
                            //in_chain_mu.GetMaximum(var_name));
                            var_min,
                            var_max);

  histo_mu -> SetLineColor(kGreen);
  histo_pi -> SetLineColor(kRed);
  histo_e -> SetLineColor(kBlue);
  histo_k -> SetLineColor(kMagenta);

  histo_mu -> SetLineWidth(2);
  histo_pi -> SetLineWidth(2);
  histo_e -> SetLineWidth(2);
  histo_k -> SetLineWidth(2);

  histo_mu -> SetStats(0);
  histo_pi -> SetStats(0);
  histo_e -> SetStats(0);
  histo_k -> SetStats(0);

  histo_mu  -> SetTitle("muons");
  histo_pi  -> SetTitle("pions");
  histo_k  -> SetTitle("kaons");
  histo_e  -> SetTitle("electrons");

  histo_mu -> GetXaxis()-> SetTitle(titlet);
  histo_pi -> GetXaxis()-> SetTitle(titlet);
  histo_e -> GetXaxis()-> SetTitle(titlet);
  histo_k -> GetXaxis()-> SetTitle(titlet);

  for(size_t irow = 0 ; irow < in_chain_mu.GetEntries() ; ++irow){
    in_chain_mu.GetEntry(irow);
    histo_mu -> Fill(var_value);
  }
  for(size_t irow = 0 ; irow < in_chain_pi.GetEntries() ; ++irow){
    in_chain_pi.GetEntry(irow);
    histo_pi -> Fill(var_value);
  }
  for(size_t irow = 0 ; irow < in_chain_e.GetEntries() ; ++irow){
    in_chain_e.GetEntry(irow);
    histo_e -> Fill(var_value);
  }
  for(size_t irow = 0 ; irow < in_chain_k.GetEntries() ; ++irow){
    in_chain_k.GetEntry(irow);
    histo_k -> Fill(var_value);
  }

 // histo_mu -> Scale(0.03);

  auto c = new TCanvas("Variable Plots");

  //c->SetLogy();

  histo_e -> DrawNormalized();
  histo_pi -> DrawNormalized("SAME");
  histo_mu -> DrawNormalized("SAME");
  histo_k -> DrawNormalized("SAME");

  gStyle->SetOptTitle(0); // Remoces title bar

 //  TCanvas *c = new TCanvas("c","stacked hists");
 //  hs->Draw("nostack");
 //  return c;

  //auto l = new TLegend();
  //l -> AddEntry(histo_mu,"muons","l");
  //l -> AddEntry(histo_pi,"pions","l");
  //l -> Draw();
  c->BuildLegend();
}
