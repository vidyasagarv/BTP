import ROOT

# in order to start TMVA
ROOT.TMVA.Tools.Instance()

# open input file, get trees, create output file
input = ROOT.TFile("allmom.root")
tree_s = input.Get("Mu")
tree_ba = input.Get("E")
tree_bb = input.Get("K")
tree_bc = input.Get("Pi")
fout = ROOT.TFile("BDT.root","RECREATE")

# define factory with options
factory = ROOT.TMVA.Factory("TMVAClassification", fout,
                            ":".join(["!V",
                                      "!Silent",
                                      "Color",
                                      "DrawProgressBar",
                                      "AnalysisType=Classification"]
                                     ))

dataloader = ROOT.TMVA.DataLoader('dataset')
# add discriminating variables for training
dataloader.AddVariable("ecledivp", 'F')
dataloader.AddVariable("ecle9e25", 'F')
dataloader.AddVariable("eclncr", 'F')
dataloader.AddVariable("Idtof", 'F')
dataloader.AddVariable("Idacc", 'F')
dataloader.AddVariable("dedxdive", 'F')
dataloader.AddVariable("Klmlm", 'F')
dataloader.AddVariable("Klmlk", 'F')
dataloader.AddVariable("Klmlp", 'F')
dataloader.AddVariable("Klmoc", 'F')
dataloader.AddVariable("Klmnh", 'F')
dataloader.AddSpectator("mom",'F')
dataloader.AddSpectator("muid",'F')
dataloader.AddSpectator("tag",'F')

signalWeight = 1.0
backgroundWeight = 1.0

# define signal and background trees
dataloader.AddSignalTree(tree_s, signalWeight)
dataloader.AddBackgroundTree(tree_ba, backgroundWeight)
dataloader.AddBackgroundTree(tree_bb, backgroundWeight)
dataloader.AddBackgroundTree(tree_bc, backgroundWeight)

# define additional cuts
sigCut = ROOT.TCut("")
bgCut = ROOT.TCut("")

# set options for trainings
dataloader.PrepareTrainingAndTestTree(sigCut,
                                   bgCut,
                                   ":".join(["SplitMode=Random",
                                             "NormMode=NumEvents",
                                             "!V"
                                   ]))

method = factory.BookMethod(dataloader, ROOT.TMVA.Types.kBDT, "BDT",
                            ":".join(["!H",
                                      "!V",
                                      "NTrees=850",
                                      "MinNodeSize=2.5%",
                                      "MaxDepth=3",
                                      "BoostType=AdaBoost",
                                      "AdaBoostBeta=0.5",
                                      "UseBaggedBoost",
                                      "BaggedSampleFraction=0.5",
                                      "SeparationType=GiniIndex",
                                      "nCuts=20",
                                      ]))

# self-explaining
factory.TrainAllMethods()
factory.TestAllMethods()
factory.EvaluateAllMethods()

# sSave the output file
fout.Close()

# open the GUI for the result macros
gui = ROOT.TMVA.TMVAGui("BDT.root");

# keep the ROOT thread running
ROOT.gApplication.Run()

#raw_input('Press <ret> to end -> ')
