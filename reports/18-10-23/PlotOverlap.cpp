void PlotOverlap(const char *var_name, int no_bins = 100){

  TChain in_chain_mu("Mu");
  TChain in_chain_pi("Pi");

  in_chain_mu.Add("./../../analysis/allmom.root/Mu");
  in_chain_pi.Add("./../../analysis/allmom.root/Pi");

  float var_value;

  in_chain_mu.SetBranchAddress(var_name, &var_value);
  in_chain_pi.SetBranchAddress(var_name, &var_value);

  TH1F *histo_mu = new TH1F("Mu",
                            var_name,
                            no_bins,
                            //in_chain_mu.GetMinimum(var_name),
                            //in_chain_mu.GetMaximum(var_name));
                            0,
                            5);
  TH1F *histo_pi = new TH1F("Pi",
                            var_name,
                            no_bins,
                            //in_chain_pi.GetMinimum(var_name),
                            //in_chain_pi.GetMaximum(var_name));
                            0,
                            5);

  histo_mu -> SetLineColor(kRed);
  histo_pi -> SetLineColor(kCyan);

  histo_mu -> SetLineWidth(3);
  histo_pi -> SetLineWidth(3);

  histo_mu -> SetStats(0);
  histo_pi -> SetStats(0);

  histo_mu -> SetTitle("");
  histo_pi -> SetTitle("");

  for(size_t irow = 0 ; irow < 100000 ; ++irow){
    in_chain_mu.GetEntry(irow);
    histo_mu -> Fill(var_value);
    in_chain_pi.GetEntry(irow);
    histo_pi -> Fill(var_value);
  }

  auto c = new TCanvas("Variable Plots");

  //c->SetLogy();

  histo_pi -> DrawNormalized();
  histo_mu -> DrawNormalized("SAME");

 //  TCanvas *c = new TCanvas("c","stacked hists");
 //  hs->Draw("nostack");
 //  return c;
}
